package main

import(
	"fmt"
	"os"
	"time"
)

func main(){
	start := time.Now()
	
	for index,arg:=range os.Args[1:]{
	  fmt.Printf("%d",index)
	  fmt.Printf("-%s\n",arg);
	}
	
	secs := time.Since(start).Seconds()
	
	fmt.Printf("total seconds:%d",secs)
}