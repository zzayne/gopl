package main

import(
	"fmt"
	"net/http"
	"io"
	"os"
)

func main(){
	for _,url:=range os.Args[1:]{
		reps,err:=http.Get(url)
		if err!=nil{
			fmt.Fprintf(os.Stderr,"fetch:%v\n",err)
			os.Exit(1)
		}
		
		io.Copy(os.Stdout, reps.Body)
		//b,err:=ioutil.ReadAll(reps.Body)
		reps.Body.Close()		
	}
}