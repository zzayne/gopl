package main

import(
	"fmt"
	"os"
	"time"
	"strings"
)

func main(){
	
	
	args:=os.Args[1:]
	s, sep := "", ""

	start := time.Now()

    for _, arg := range args {
        s += sep + arg
        sep = " "
    }
	fmt.Println(s)
	
	secs := time.Since(start).Seconds()
	
	fmt.Printf("fun one total seconds:%g",secs)
	fmt.Printf("\n")
	
	start_2 := time.Now()
	
	fmt.Println(strings.Join(os.Args[1:], " "))
	
	secs_2 := time.Since(start_2).Seconds()
	
	fmt.Printf("fun two total seconds:%g",secs_2)
	
}